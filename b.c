#include<stdio.h>
//Program to find the Sequence of Fibonacci

int fibonacci(int); //Function to find fibonacci
int fibonacciSeq(int); //Function to get the sequence of fibonacci
int c=0;
int n=0;

int main()
{
  int num;
  printf("Enter the number :\n");
  scanf("%d",&num);
  printf("Fibonacci Sequence of %d is: \n",num);
  fibonacciSeq(num);
  return 0;
}

int fibonacci(int a)
{
    if(a==0)
        return 0;
    else if(a==1)
        return 1;
    else
        return fibonacci(a-1)+fibonacci(a-2);
}

int fibonacciSeq(int b)
{
    if(b>=c)
    {
        printf("%d \n",fibonacci(n));
        n++;
        c++;
        fibonacciSeq(b);
    }
    else return 0;
}
